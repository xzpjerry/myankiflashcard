
# Final Project: MyAnKi FlashCard in C++

## Introduction

"AnKi is a  a program which makes remembering things easy. Because it's a lot more efficient than traditional study methods, you can either greatly decrease your time spent studying, or greatly increase the amount you learn." ([Link to its website](https://apps.ankiweb.net))

## Why choose it as my imitation object?

- I know every detail of it. I have used it for about 6 months to study GRE words, manging 10,000 words with multiple media files including pictures and sounds.

## Functionalities

- For this final project, I am going to build a console based Anki that can helps users manage their flashcards, letting them review cards in time with ease.
- It should be able to present images, thought I think it might be tricky to do so in a console, and play recorded audio.
